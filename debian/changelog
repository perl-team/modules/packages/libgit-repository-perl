libgit-repository-perl (1.325-3) unstable; urgency=medium

  * Team upload.
  * Remove libmodule-build-perl from Build-Depends.
    This distribution uses ExtUtils::MakeMaker.
  * Add a patch to skip two subtests which fail with Git ≥ 2.40.0.
    The tests expect (error) messages which were removed in Git, so don't
    check for them with a new enough Git.
    The test failures caused by these subtests were observed
    during build and autopkgtests.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Wed, 22 Mar 2023 17:15:32 +0100

libgit-repository-perl (1.325-2) unstable; urgency=medium

  * Team upload.
  * Add patch from GitHub pull request to fix test suite with
    Git ≥ 2.38.1. (Closes: #1023597)
  * Declare compliance with Debian Policy 4.6.1.

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 Nov 2022 14:18:15 +0100

libgit-repository-perl (1.325-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libsystem-command-perl.
    + libgit-repository-perl: Drop versioned constraint on
      libsystem-command-perl in Depends.

  [ gregor herrmann ]
  * Import upstream version 1.325.
  * Drop git-2.30.0.patch, fixed in this release.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Tue, 21 Sep 2021 19:25:29 +0200

libgit-repository-perl (1.324-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Add patch to workaround Git v2.30.0's friendly hints which cause test
    failures in t/24-errors.t. (Closes: #978325)
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Fri, 01 Jan 2021 01:12:59 +0100

libgit-repository-perl (1.324-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.324.
  * Update years of upstream copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.
  * Fix hashbang in example script.

 -- gregor herrmann <gregoa@debian.org>  Mon, 02 Sep 2019 00:00:53 +0200

libgit-repository-perl (1.323-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.323.
  * Drop git-2.19-errors.patch, fixed in this release.

 -- gregor herrmann <gregoa@debian.org>  Mon, 26 Nov 2018 19:35:29 +0100

libgit-repository-perl (1.322-2) unstable; urgency=medium

  * Team upload.
  * Add patch to fix tests with changed error messages from git 2.19.
    Thanks to ci.debian.net.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Wed, 29 Aug 2018 17:34:44 +0200

libgit-repository-perl (1.322-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.322
  * Bump debhelper compatibility level to 11
  * Declare compliance with Debin Policy 4.1.5
  * Update years of upstream copyright
  * Drop unneeded patch, fixed by upstream

 -- Lucas Kanashiro <kanashiro@debian.org>  Thu, 26 Jul 2018 02:30:42 -0300

libgit-repository-perl (1.321-2) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Add patch to adjust test to changed Git error messages.
    Patch taken from upstream pull request. (Closes: #894727)
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Tue, 03 Apr 2018 19:06:20 +0200

libgit-repository-perl (1.321-1) unstable; urgency=medium

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Damyan Ivanov ]
  * New upstream version 1.321
  * add myself to Uploaders
  * update years of upstream copyright
  * declare conformance with Policy 4.1.1

 -- Damyan Ivanov <dmn@debian.org>  Wed, 25 Oct 2017 12:37:15 +0000

libgit-repository-perl (1.320-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.320
  * Adjust required version for libsystem-command-perl to (>= 1.118)

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 22 Jul 2016 13:43:22 +0200

libgit-repository-perl (1.319-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ Salvatore Bonaccorso ]
  * Import upstream version 1.319
  * Add Build-Depends-Indep on libtest-requires-git-perl

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 22 May 2016 12:41:32 +0200

libgit-repository-perl (1.318-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Import upstream version 1.318

  [ gregor herrmann ]
  * Drop unneeded version constraint from libmodule-build-perl build
    dependency. Already satisfied in oldstable.
  * Add new (build) dependencies.
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Sun, 01 May 2016 11:27:37 +0200

libgit-repository-perl (1.317-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Import upstream version 1.317.
  * Drop spelling.patch, merged upstream.
  * Update years of upstream copyright.
  * Install new example script.
  * Declare compliance with Debian Policy 3.9.7.

 -- gregor herrmann <gregoa@debian.org>  Mon, 15 Feb 2016 20:21:03 +0100

libgit-repository-perl (1.316-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.316
  * Drop Git-Repository-1.315-git-2.5.2-compat.patch which was taken from
    upstream bug tracker.
  * Bump debhelper compatibility level to 9.
  * Add a patch to fix a spelling mistake in the POD.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Dec 2015 21:06:09 +0100

libgit-repository-perl (1.315-2) unstable; urgency=high

  * Team upload.
  * Fix test suite failure with newer git versions. Patch by Petr Šabata.
    (Closes: #801499)

 -- Niko Tyni <ntyni@debian.org>  Sun, 11 Oct 2015 14:18:30 +0300

libgit-repository-perl (1.315-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.315

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 02 Aug 2015 17:15:39 +0200

libgit-repository-perl (1.314-1) unstable; urgency=medium

  * Team upload.
  * Add debian/upstream/metadata.
  * Import upstream version 1.314.
  * Update years of upstream copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Jul 2015 21:28:40 +0200

libgit-repository-perl (1.312-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 1.312
  * Wrap and sort fields in debian/control

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 24 Aug 2014 09:34:01 +0200

libgit-repository-perl (1.310-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 1.310
  * Update copyright years for upstream files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 26 Jan 2014 21:59:42 +0100

libgit-repository-perl (1.309-2) unstable; urgency=medium

  * Team upload.
  * Add libgit-repository-plugin-log-perl to Recommends and mention it in
    debian/NEWS.

 -- gregor herrmann <gregoa@debian.org>  Mon, 30 Dec 2013 16:27:50 +0100

libgit-repository-perl (1.309-1) unstable; urgency=low

  * Team upload

  * Imported Upstream version 1.309
  * drop trailing slash from metacpan URLs
  * claim conformance with Policy 3.9.5

 -- Damyan Ivanov <dmn@debian.org>  Sat, 09 Nov 2013 10:53:20 +0200

libgit-repository-perl (1.308-1) unstable; urgency=low

  * Team upload

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Nuno Carvalho ]
  * New upstream release
  * d/copyright: update copyright years
  * d/control:
    + add minimum required version for libsystem-command-perl
    + remove libtest-pod* from D-B-I, tests are not executed.
  * Add NEWS file to alert for deprecated items.

 -- Nuno Carvalho <smash@cpan.org>  Mon, 30 Sep 2013 15:27:53 +0100

libgit-repository-perl (1.29-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 1.29

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 07 Dec 2012 17:00:23 +0100

libgit-repository-perl (1.28-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 1.28
  * Drop fix-ident-in-test.patch patch.
    Patch was applied upstream.

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 05 Nov 2012 18:06:06 +0100

libgit-repository-perl (1.27-1) unstable; urgency=low

  * New upstream release
  * Update upstream copyright years
  * Bump Standards-Version to 3.9.4 (no changes needed)

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 16 Oct 2012 14:01:55 +0200

libgit-repository-perl (1.26-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Alessandro Ghedini ]
  * New upstream release
  * Email change: Alessandro Ghedini -> ghedo@debian.org
  * Update copyright to Copyright-Format 1.0
  * Bump Standards-Version to 3.9.3 (no changes needed)
  * Add fix-ident-in-test.patch

 -- Alessandro Ghedini <ghedo@debian.org>  Wed, 01 Aug 2012 12:05:13 +0200

libgit-repository-perl (1.25-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 1.25
  * Bump again the Build-Depends for Module::Build.
    Change the Build-Depends to
    perl (>= 5.13.11) | libmodule-build-perl (>= 0.380000).

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 28 Dec 2011 17:13:15 +0100

libgit-repository-perl (1.24-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 1.24
  * Drop fix-spelling.patch.
    Patch is applied upstream.
  * Lower requirements on Module::Build.
    Change alternate Build-Depends for Module::Build to
    perl (>= 5.11.3) | libmodule-build-perl (>= 0.360000).

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 26 Dec 2011 18:00:36 +0100

libgit-repository-perl (1.23-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Bump Module::Build build dependency.

 -- gregor herrmann <gregoa@debian.org>  Wed, 07 Dec 2011 19:06:20 +0100

libgit-repository-perl (1.22-1) unstable; urgency=low

  * Initial Release (Closes: #645914)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sat, 22 Oct 2011 13:12:24 +0200
